#!/bin/bash
set -e

echo $BASH_SOURCE
cd "$(dirname "$(readlink -f "$BASH_SOURCE")")"

set -x
docker build -f Dockerfile.build -t mini-container-builder .
docker run --rm mini-container-builder cat hello > hello
chmod +x hello
# ./hello > /dev/null
docker build -t mini-container .
docker run --rm mini-container
