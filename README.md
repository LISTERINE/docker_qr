# docker_qr

Put a mini docker conatiner into a QR code

### Build the mini container
Enter the mini-container directory and edit the hello.asm file to your liking.

run: ``` ./update```

### Generate the QR code
In the top level directory run: ```./save```

Now check the qrcodes directory for your png

### Run the container
For completeness, remove mini-container if you already have it: ```docker rmi mini-container:latest```
Scan the QR code and run the string you get back in bash. It will deserialize the container, load it, and run it.