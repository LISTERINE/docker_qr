#!/bin/bash
for f in /qrcodes/*.qr; do
	qrencode "$(cat $f)" -o "$f.png"
	echo created "$f.png"
done
