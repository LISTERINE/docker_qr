FROM ubuntu:latest

RUN apt-get update -y && apt-get install -y qrencode

WORKDIR /

ADD make_qr_codes.sh /make_qr_codes.sh

CMD ["/bin/bash", "/make_qr_codes.sh"]
